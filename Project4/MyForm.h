#pragma once
#include <cmath>
#include <ctime>
#include <string>
#include <fstream>
#include <iostream>
#include <windows.h>
#include <stdio.h>

double arrFor[10] = {}; 
double arrWhile[10] = {}; 
double arrDoWhile[10] = {};
int arrAssInt[10] = {};
double arrAssDouble[10] = {};
double arrLoopAssDouble[1000] = {};

namespace Project4 {

	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected:
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart1;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Button^  button8;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series6 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series7 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series8 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Title^  title2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(31, 304);
			this->button1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(100, 28);
			this->button1->TabIndex = 0;
			this->button1->Text = L"test for";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(159, 304);
			this->button2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(100, 28);
			this->button2->TabIndex = 1;
			this->button2->Text = L"test while";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(287, 304);
			this->button3->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(100, 28);
			this->button3->TabIndex = 2;
			this->button3->Text = L"test do while";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// chart1
			// 
			chartArea2->Name = L"ChartArea1";
			this->chart1->ChartAreas->Add(chartArea2);
			legend2->Name = L"Legend1";
			this->chart1->Legends->Add(legend2);
			this->chart1->Location = System::Drawing::Point(31, 36);
			this->chart1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->chart1->Name = L"chart1";
			series4->ChartArea = L"ChartArea1";
			series4->Legend = L"Legend1";
			series4->Name = L"for";
			series5->ChartArea = L"ChartArea1";
			series5->Legend = L"Legend1";
			series5->Name = L"while";
			series6->ChartArea = L"ChartArea1";
			series6->Legend = L"Legend1";
			series6->Name = L"do while";
			series7->ChartArea = L"ChartArea1";
			series7->Legend = L"Legend1";
			series7->Name = L"assign int";
			series8->ChartArea = L"ChartArea1";
			series8->Legend = L"Legend1";
			series8->Name = L"assign double";
			this->chart1->Series->Add(series4);
			this->chart1->Series->Add(series5);
			this->chart1->Series->Add(series6);
			this->chart1->Series->Add(series7);
			this->chart1->Series->Add(series8);
			this->chart1->Size = System::Drawing::Size(1119, 245);
			this->chart1->TabIndex = 5;
			this->chart1->Text = L"chart1";
			title2->Name = L"Pomiar czasu";
			this->chart1->Titles->Add(title2);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(645, 304);
			this->button4->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(100, 62);
			this->button4->TabIndex = 4;
			this->button4->Text = L"zaladuj wyniki";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(784, 304);
			this->button5->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(100, 62);
			this->button5->TabIndex = 5;
			this->button5->Text = L"wylicz srednia";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(929, 321);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(46, 17);
			this->label1->TabIndex = 6;
			this->label1->Text = L"label1";
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(449, 315);
			this->button6->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(135, 41);
			this->button6->TabIndex = 7;
			this->button6->Text = L"datalog";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(47, 344);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(105, 30);
			this->button7->TabIndex = 8;
			this->button7->Text = L"test assign int";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &MyForm::Assign_Integer);
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(196, 344);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(166, 29);
			this->button8->TabIndex = 9;
			this->button8->Text = L"test assign double";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &MyForm::Assign_Double);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1192, 385);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->chart1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

		double timeStart;
		double timeForSec, timeWhileSec, timeDoWhileSec, timeAssIntSec, timeAssDoubleSec;
		long iFor, iWhile, iDoWhile, iAssInt, iAssDouble;

		int whichElFor = 0;
		int whichElWhile = 0; 
		int whichElDoWhile = 0; 
		int whichElAssInt = 0; 
		int whichElAssDouble = 0;

		int t_h, t_m, t_s, t_ms;
		
		int saveTime() {
			SYSTEMTIME st;
			GetSystemTime(&st);
			t_h = st.wHour; t_m = st.wMinute; t_s = st.wSecond; t_ms = st.wMilliseconds;
			return 0;
		}	
		int saveLogFor() {
			saveTime();
			std::ofstream myfile;
			myfile.open("datalog.txt", std::ofstream::app);
			myfile << "FOR -- The system time: " << t_h << ":" << t_m << ":" << t_s << ":" << t_ms << "   wynik : " << timeForSec << "\n";
				myfile.close();
			return 0;
		}
		int saveLogWhile() {
			saveTime();
			std::ofstream myfile;
			myfile.open("datalog.txt", std::ofstream::app);
			myfile << "WHILE -- The system time: " << t_h << ":" << t_m << ":" << t_s << ":" << t_ms << "   wynik : " << timeWhileSec << "\n";
				myfile.close();
			return 0;
		}
		int saveLogDoWhile() {
			saveTime();
			std::ofstream myfile;
			myfile.open("datalog.txt", std::ofstream::app);
			myfile << "DO WHILE -- The system time: " << t_h << ":" << t_m << ":" << t_s << ":" << t_ms << "   wynik : " << timeDoWhileSec << "\n";
				myfile.close();
			return 0;
		}
		int saveLogAssInt() {
			saveTime();
			std::ofstream myfile;
			myfile.open("datalog.txt", std::ofstream::app);
			myfile << "Assigning int -- The system time: " << t_h << ":" << t_m << ":" << t_s << ":" << t_ms << "   wynik : " << timeAssIntSec << "\n";
			myfile.close();
			return 0;
		}
		int saveLogAssDouble() {
			saveTime();
			std::ofstream myfile;
			myfile.open("datalog.txt", std::ofstream::app);
			myfile << "Assigning double -- The system time: " << t_h << ":" << t_m << ":" << t_s << ":" << t_ms << "   wynik : " << timeAssDoubleSec << "\n";
			myfile.close();
			return 0;
		}



#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		timeStart = clock();
		for (iFor = 0; iFor <= 10000000; iFor++) { }
		timeForSec = (clock() - timeStart) / (double)CLOCKS_PER_SEC;

		arrFor[whichElFor] = timeForSec;
		whichElFor++;
		if (whichElFor == 9) { whichElFor = 0; }
		//this->chart1->Series["for"]->Points->AddXY("time", timeForSec);
		saveLogFor();
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		timeStart = clock();
		while (iWhile <= 10000000) { iWhile++; }
		timeWhileSec = (clock() - timeStart) / (double)CLOCKS_PER_SEC;

		arrWhile[whichElWhile] = timeWhileSec;
		whichElWhile++;
		if (whichElWhile == 9) { whichElWhile = 0; }
		//this->chart1->Series["while"]->Points->AddXY("time", timeWhileSec);
		iWhile = 0;
		saveLogWhile();
	}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
		timeStart = clock();
		do { iDoWhile++; } while (iDoWhile <= 10000000);
		timeDoWhileSec = (clock() - timeStart) / (double)CLOCKS_PER_SEC;

		arrDoWhile[whichElDoWhile] = timeDoWhileSec;
		whichElDoWhile++;
		if (whichElDoWhile == 9) { whichElDoWhile = 0; }
		//this->chart1->Series["do while"]->Points->AddXY("time", timeDoWhileSec);
		iDoWhile = 0;
		saveLogDoWhile();
	}
private: System::Void Assign_Integer(System::Object^  sender, System::EventArgs^  e) {
		timeStart = clock();
		do { iAssInt; } while (iAssInt <= 10000000);
		timeAssIntSec = (clock() - timeStart) / (double)CLOCKS_PER_SEC;

		arrAssInt[whichElAssInt] = timeAssIntSec;
		whichElAssInt++;
		if (whichElAssInt == 9) { whichElAssInt = 0; }
		//this->chart1->Series["do while"]->Points->AddXY("time", timeDoWhileSec);
		iAssInt = 0;
		saveLogAssInt();
	}
private: System::Void Assign_Double(System::Object^  sender, System::EventArgs^  e) {
		timeStart = clock();
		
		do { arrLoopAssDouble[iAssDouble] = 123123; iAssDouble++; } while (iAssDouble <= 10000000);
		timeAssDoubleSec = (clock() - timeStart) / (double)CLOCKS_PER_SEC;

		arrAssDouble[whichElAssDouble] = timeDoWhileSec;
		whichElAssDouble++;
		if (whichElAssDouble == 9) { whichElAssDouble = 0; }
		//this->chart1->Series["do while"]->Points->AddXY("time", timeDoWhileSec);
		iAssDouble = 0;
		saveLogAssDouble();
	}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	whichElFor--; whichElWhile--; whichElDoWhile--; whichElAssInt--; whichElAssDouble--;
		timeForSec = arrFor[whichElFor];
		timeWhileSec = arrWhile[whichElWhile];
		timeDoWhileSec = arrDoWhile[whichElDoWhile];
		timeAssIntSec = arrAssInt[whichElAssInt];
		timeAssDoubleSec = arrAssDouble[whichElAssDouble];
	


		this->chart1->Series["for"]->Points->AddXY("time", timeForSec);
		this->chart1->Series["while"]->Points->AddXY("time", timeWhileSec);
		this->chart1->Series["do while"]->Points->AddXY("time", timeDoWhileSec);
		this->chart1->Series["assign int"]->Points->AddXY("time", timeAssIntSec);
		this->chart1->Series["assign double"]->Points->AddXY("time", timeAssDoubleSec);

		whichElFor++; whichElWhile++; whichElDoWhile++; whichElAssInt++; whichElAssDouble++;

	}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	int i; double avFor; double avWhile; double avDoWhile; string avForString;
	int avAssInt; double avAssDouble;
		for (i = 0; i < 10; i++) {
			avFor = avFor + arrFor[i];
			avWhile = avWhile + arrWhile[i];
			avDoWhile = avDoWhile + arrDoWhile[i];
			avAssInt = avAssInt + arrAssInt[i];
			avAssDouble = avAssDouble + arrAssDouble[i];
			
		}
		avFor = avFor / 10;
		avWhile = avWhile / 10;
		avDoWhile = avDoWhile / 10;
		avAssInt = avAssInt / 10;
		avAssDouble = avAssDouble / 10;

		std::ofstream myfile;
		myfile.open("datalog.txt", std::ofstream::app);
		myfile << "\n Srednia FOR: " << avFor << "\n Srednia WHILE: " << avWhile << "\n Srednia DO WHILE: " << avDoWhile << "\n Srednia ASSIGN INT: " << avAssInt << "\n Srednia ASSIGN DOUBLE: " << avAssDouble << "\n";
		myfile.close();

		label1->Text = "Srednia znajduje sie w datalogu.";

	}

private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	system("START C:\datalog.txt");
}
};
}
